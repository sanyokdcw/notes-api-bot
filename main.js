const TelegramBot = require('node-telegram-bot-api');
const token = '1821312101:AAHIvE-nykJ8KhHzje06DsgUtG7x1VirM84';
const bot = new TelegramBot(token, {polling: true});

const axios = require('axios').default;
axios.defaults.baseURL = 'https://api.notes.a-lux.dev/api/';

let inline_keyboard = [];

let tags = {}
let notes = {}

let settings = {
    241901437: '1|orL7rV8zRa9xNHI5MTh8Igf8tbHsF6cZix6CZtEI',
};

bot.onText(/\/start/, (msg, [source, match]) => {
    const {chat: {id}} = msg
    bot.sendMessage(id, `Введите ваш токен`, {
        reply_markup: {
            force_reply: true
        }
    }).then(addChatToken => {
        bot.onReplyToMessage(addChatToken.chat.id, addChatToken.message_id, msg => {
            settings[`${msg.chat.id}`] = '1|orL7rV8zRa9xNHI5MTh8Igf8tbHsF6cZix6CZtEI'
            bot.sendMessage(addChatToken.chat.id, 'Токен сохранен')
            bot.sendMessage(addChatToken.chat.id, 'Введите /tags, чтобы посмотреть свои метки')
        })
    })
});

bot.onText(/\/tags/, (msg, [source, match]) => {

    const config = {
        headers: {Authorization: `Bearer ${settings[`${msg.chat.id}`]}`}
    };

    axios.get('tags', config)
        .then(function (response) {
            inline_keyboard = []
            tags = response.data.tags
            for (let i = 0; i < tags.length; i++) {
                inline_keyboard.push(
                    [
                        {
                            text: tags[i].name,
                            callback_data: 'TAG_' + tags[i].name + '_' + tags[i].id
                        }
                    ]
                )
            }
        })
        .then(function () {
            bot.sendMessage(msg.chat.id, `Выберите метку, чтобы посмотреть связанные заметки`, {
                reply_markup: {
                    inline_keyboard
                }
            })
        });
});

bot.on('callback_query', query => {
    const {message: {chat, message_id, text} = {}} = query
    if (query.data.includes('TAG')) {
        bot.sendMessage(query.message.chat.id, `Заметки c меткой: <b>${query.data.split('_')[1]} </b>`, {parse_mode: "HTML"})

        axios.get('tags/' + query.data.split('_')[2], {
            headers: {Authorization: `Bearer ${settings[`${query.message.chat.id}`]}`},
        })
            .then(function (response) {
                notes = response.data.notes;
            }).then(() => {
                for (let i = 0; i < notes.length; i++) {
                    bot.sendMessage(query.message.chat.id, `<b>${notes[i].name}</b>\n${notes[i].text}`, {parse_mode: "HTML"})
                }
            }
        )
    }
})

bot.onText(/\/token/, (msg, [source, match]) => {
    bot.sendMessage(msg.chat.id, 'Ваш токен: ' + settings[`${msg.chat.id}`])
});

bot.on('polling_error', (error) => {
    console.log(error);
});